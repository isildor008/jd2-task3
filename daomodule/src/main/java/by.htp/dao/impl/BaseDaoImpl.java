package by.htp.dao.impl;


import org.hibernate.Session;
import org.hibernate.SessionFactory;


/**
 * Created by Dmitry on 09.09.2017.
 */

public class BaseDaoImpl {

    SessionFactory factory = new org.hibernate.cfg.Configuration().configure("hibernate.cfg.xml").buildSessionFactory();


    public void save(Object o) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            session.close();
        }
    }


    public void update(Object o) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
        } finally {
            session.close();
        }

    }

    public void delete(Object o) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
        }finally {
            session.close();
        }
    }

}
