package by.htp.entity;

import java.sql.Timestamp;

/**
 * Created by Dmitry on 23.09.2017.
 */
public class NewsBuilder {
    private int id;
    private String title;
    private Timestamp date;
    private String brief;
    private String content;
    private Boolean status;

    NewsBuilder buiildId(int id){
        this.id=id;
        return this;    }


    NewsBuilder buiildTitle(String title){
        this.title=title;
        return this;    }

    NewsBuilder buiildDate(Timestamp date){
        this.date=date;
        return this;    }

    NewsBuilder buiildBrief(String brief){
        this.brief=brief;
        return this;    }

    NewsBuilder buiildContent(String content){
        this.content=content;
        return this;    }

    NewsBuilder buiildStatus(Boolean status){
        this.status=status;
        return this;    }

       public News build(){
        News news=new News();
        news.setId(id);
        news.setTitle(title);
        news.setBrief(brief);
        news.setContent(content);
        news.setStatus(status);
        return news;
        }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsBuilder that = (NewsBuilder) o;

        if (id != that.id) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (brief != null ? !brief.equals(that.brief) : that.brief != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (brief != null ? brief.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsBuilder{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", date=" + date +
                ", brief='" + brief + '\'' +
                ", content='" + content + '\'' +
                ", status=" + status +
                '}';
    }
}
