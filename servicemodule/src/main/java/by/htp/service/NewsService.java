package by.htp.service;


import by.htp.dao.NewsDao;


import by.htp.entity.News;
import by.htp.entity.NewsBuilder;
import by.htp.service.serviceExcrption.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import static by.htp.service.serviceExcrption.ConstantService.EXCEPTIONADDMESSAGE;
import static by.htp.service.serviceExcrption.ConstantService.EXCEPTIONDELETEMESSAGE;
import static by.htp.service.serviceExcrption.ConstantService.EXRPTIONGETBYID;

@Service("newsService")
@Transactional
public class NewsService {
    @Autowired
    NewsDao newsDao;

    public List<News> getAllNews() {
        List<News> newsList = newsDao.getAllNews();
        return newsList;
    }

    public News getNewsById(int id) {
        if (0!= id) {
            News news = newsDao.getById(id);
            return news;
        } else {
            throw  new SecurityException(EXRPTIONGETBYID);
        }
    }


    public void delete(int id) {
        if (0!= id) {
            News news = newsDao.getById(id);
            news.setStatus(false);
            newsDao.update(news);
        } else {
            try {
                throw new ServiceException(EXCEPTIONDELETEMESSAGE);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
    }

    public void add(NewsBuilder newsBuilder){
        if(null!=newsBuilder){
            newsDao.save(newsBuilder.build());}
        else {
            try {
                throw new ServiceException(EXCEPTIONADDMESSAGE);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        }
    }


}
