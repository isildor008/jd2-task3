package by.htp.webpr.controller;


import by.htp.entity.NewsBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import by.htp.service.NewsService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NewsManager {

    @Autowired
    private NewsService newsService;

    @RequestMapping(value = "/newsList", method = RequestMethod.GET)
    public String newsList(Model model) {
        model.addAttribute("newsList", newsService.getAllNews());
        return "newsListPage";
    }

    @RequestMapping(value = "/editNews", method = RequestMethod.GET)
    public String editNewsGet(Model model, @RequestParam int id) {
        model.addAttribute("news", newsService.getNewsById(id));
        return "newsEditPage";
    }

    @RequestMapping(value = "/editNews", method = RequestMethod.POST)
    public String editNewsPost(Model model, @RequestParam int id) {
        model.addAttribute("news", newsService.getNewsById(id));
        return "redirect:/newsEditPage";
    }

    @RequestMapping(value = "/viewNews", method = RequestMethod.GET)
    public String viewNewsGet(Model model, @RequestParam int id) {
        model.addAttribute("news", newsService.getNewsById(id));
        return "newsViewPage";
    }

    @RequestMapping(value = "/deleteNews", method = RequestMethod.DELETE)
    public String deleteNews(@RequestParam int id) {
        newsService.delete(id);
        return "redirect:/newsEditPage";
    }

    @RequestMapping(value = "/addNews", method = RequestMethod.GET)
    public String addNewsGet(Model model) {
        model.addAttribute("category", new NewsBuilder());
        return "redirect:/addNewsPage";
    }

    @RequestMapping(value = "addNews", method = RequestMethod.PUT)
    public String addNewsPost(@ModelAttribute("user") NewsBuilder newsBuilder) {
        newsService.add(newsBuilder);
        return "redirect:/addNewsPage";
    }
}
