<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <%@include file="/WEB-INF/static/common_css.jsp" %>
    <%@include file="/WEB-INF/static/common_js.jsp" %>
    <title></title>
</head>
<body>
<%@include file="/WEB-INF/tile/header.jsp" %>
<%@include file="/WEB-INF/tile/menuNavigation.jsp" %>
<%@include file="/WEB-INF/tile/newsListTile.jsp" %>
</body>
</html>
