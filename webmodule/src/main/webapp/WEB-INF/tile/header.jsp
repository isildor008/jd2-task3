<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 20.09.2017
  Time: 16:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header>
    <div class="header-title">
        <h2>News management</h2>
    </div>
    <div class="header-localization">
        <a href="#">English </a> &#160 &#160
        <a href="#">Russian</a>
    </div>
</header>