<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 20.09.2017
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="menu">
    <ul class="nav nav-tabs  nav-stacked ">
        <li><a href="#"> <i class=" icon-home"></i> News</a></li>
        <li><a href="#"><i class="icon-th-list"></i> News List</a></li>
        <li><a href="#"> <i class="icon-file"></i> Add News</a></li>
    </ul>

</section>
<section class="navigation">
    <div class="container">
        <div class="breadcrumb">
            <a href="#">News</a> <span class="divider">>></span></li>
            <a href="#" class="active">News List</a>
        </div>
</section>