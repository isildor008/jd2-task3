<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 20.09.2017
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section class="news-list">
    <div class="container">
        <c:forEach items="${newsList}" var="newsList">
        <div class="news-item">
            <a href="/newsList"></a>
            <div class="news-title">
                <a href="/viewNews?id=${newsList.getId()}"><c:out value="${newsList.getTitle()}"/></a>
            </div>
            <div class="news-date">
                <p> <c:out value="${newsList.getDate()}"/></p>
            </div>
            <div class="news-content">
                <p> <a href="/viewNews?id=${newsList.getId()}"><c:out value="${newsList.getBrief()}"/> </a></p>
            </div>
            <div class="news-actions">
                <a href="/viewNews?id=${newsList.getId()}">View</a> &#160 &#160
                <a href="/editNews?id=${newsList.getId()}">Edit</a> &#160 &#160
                <input id="checkbox" type="checkbox">
            </div>
        </div>
        </c:forEach>
    </div>
</section>