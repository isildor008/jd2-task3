<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 20.09.2017
  Time: 16:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="news-view">
    <div class="container">
        <div class="news-view-title">
            <p>News Title</p>
            <div class="news-view-title-content">
                <form:form action="viewNews" id="id" modelAttribute="news">
                <p><c:out value="${news.getTitle()}"/></p>
            </div>
        </div>
        <div class="news-view-date">
            <p>News Date </p>
            <div class="news-view-date-content">
                <p><c:out value="${news.getDate()}"/> </p>
            </div>
        </div>
        <div class="news-brief">
            <p>News Brief</p>
            <div class="news-brief-content">
                <p><c:out value="${news.getBrief()}"/></p>
            </div>
        </div>
        <div class="news-view-content-title">
            <p>Content</p>
            <div class="news-view-content">
                <p><c:out value="${news.getContent()}"/></p>
            </div>
        </div>
        <div class="news-view-buttons">
            <button class="btn   btn-primary btn-info" type="button"><a href="/deleteUser?id=${news.getId()}"></a>Edit</button>
            <button class="btn   btn-primary btn-warning" type="button">Delete</button>
        </div>
        </form:form>

    </div>
</section>